package br.gov.serpro.biblioteca;

import java.util.ArrayList;
import java.util.Collection;

public class BibliotecaHO {
	
	private Collection<UsuarioHO> usuariosRegistrados;
	private Collection<LivroHO> catalogoLivros;
	
	public BibliotecaHO() {
		this.usuariosRegistrados = new ArrayList<>();		
		this.catalogoLivros = new ArrayList<>();
	}	 
	
	public void registraUsuario(UsuarioHO usuario) {
		usuariosRegistrados.add(usuario);		
	}
	
	public int totalUsuariosRegistrados() {
		return usuariosRegistrados.size();
	}
	
	public void registraLivro(LivroHO livros) {
		catalogoLivros.add(livros);
	}
	
	public int totalLivrosCatalogo() {
		return catalogoLivros.size();
	}
	
	public boolean verificaDisponibilidadeLivro(LivroHO livroEmprestado) {
		if (livroEmprestado.usuario == null) {
			return true;
		} else {
			return false;			
		}
	}
	
	public void emprestaLivro(LivroHO livroEmprestado) {
				
		
	}
	
}
