package br.gov.serpro.biblioteca;

public class LivroHO {
	
	public int codigo;
	public String titulo;
	public String usuario;
	
	public LivroHO(int codigo, String titulo) {
		this.codigo = codigo;
		this.titulo = titulo;
	}

	public LivroHO(int codigo, String titulo, String usuario) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.usuario = usuario;
	}

}
