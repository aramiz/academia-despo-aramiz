package br.gov.serpro.biblioteca;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class BibliotecaTeste {
	
	BibliotecaHO biblioteca = new BibliotecaHO();
		
	public void informaDados() {	
		
		UsuarioHO pedro = new UsuarioHO();		
		biblioteca.registraUsuario(pedro);
		
		UsuarioHO paulo = new UsuarioHO();
		biblioteca.registraUsuario(paulo);
		
		UsuarioHO maria = new UsuarioHO();
		biblioteca.registraUsuario(maria);
		
		UsuarioHO ana = new UsuarioHO();
		biblioteca.registraUsuario(ana);
		
		UsuarioHO carla = new UsuarioHO();
		biblioteca.registraUsuario(carla);
	}
	
	public void informaDadosLivro() {
		LivroHO livro1 = new LivroHO(1, "POESIA RENACENTISTA", null);
		LivroHO livro2 = new LivroHO(2, "POESIA RENACENTISTA 2", null);
		biblioteca.registraLivro(livro1);
		biblioteca.registraLivro(livro2);		
	}
	
	@Test
	public void totalZeroUsuariosBiblioteca() {
		assertEquals(0, biblioteca.totalUsuariosRegistrados() );
	}
			
	@Test
	public void totalUsuariosMaioZero() {
		informaDados();
		assertEquals(5, biblioteca.totalUsuariosRegistrados());
	}
	
	@Test
    public void verificaLivrosCatalogovazio() {
		assertEquals(0, biblioteca.totalLivrosCatalogo());
	}
	
    @Test
    public void verificaLivrosCatalogoCheio() {
    	informaDadosLivro();    	
    	assertEquals(2, biblioteca.totalLivrosCatalogo());
    }
    
    @Test
    public void testaLivroDisponivel() { 
    	
    	
    }
    		
	
}
