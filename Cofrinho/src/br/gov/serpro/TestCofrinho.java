package br.gov.serpro;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;
import br.gov.serpro.Cofre;
import br.gov.serpro.Moeda;


public class TestCofrinho {
	Cofre meuCofre = new Cofre();
	
	@Before
	public void informaDados() {		
	
		Moeda umReal = new Moeda();	
		umReal.valorMoeda = 1.00;
	
		Moeda cincoenta = new Moeda();
		cincoenta.valorMoeda = 0.50;
	
		Moeda vinteCinco = new Moeda();
		vinteCinco.valorMoeda = 0.25;
	
		Moeda dez = new Moeda();
		dez.valorMoeda = 0.10;
	
		Moeda cinco = new Moeda();
		cinco.valorMoeda = 0.05;
	
		Moeda um = new Moeda();
		um.valorMoeda = 0.01;	

		meuCofre.depositaMoeda(umReal);
		meuCofre.depositaMoeda(vinteCinco);
		meuCofre.depositaMoeda(um);
		meuCofre.depositaMoeda(cinco);
		meuCofre.depositaMoeda(umReal);
		meuCofre.depositaMoeda(vinteCinco);
		meuCofre.depositaMoeda(cinco);
		meuCofre.depositaMoeda(dez);
		meuCofre.depositaMoeda(umReal);
		meuCofre.depositaMoeda(vinteCinco);
		meuCofre.depositaMoeda(umReal);
		meuCofre.depositaMoeda(vinteCinco);
		meuCofre.depositaMoeda(umReal);
		meuCofre.depositaMoeda(vinteCinco);
	}
	
	@Test
	public void testMaiorMoeda() {
		assertEquals("A maior moeda depositada � de: R$ 1.00", meuCofre.moedaMaiorValor());
	}

	@Test
	public void testMaiorValor() {
		assertEquals(4.96, meuCofre.valorTotal);
	}
	
	@Test
	public void testQuantMoedas() {
		assertEquals(11, meuCofre.getquantTotalMoedas());
	}
}
