package br.gov.serpro;

import java.util.Date;

public class CartaoCredito extends Pagamento {
	private int numero;
	private String tipo;
	private Date dataExpedicao;
	
	public CartaoCredito(int numero, String tipo, Date dataExpedicao ){
		this.numero = numero;
		this.tipo = tipo;
		this.dataExpedicao = dataExpedicao;
	}
	
	public boolean isAutorizado() {
		return false;
	}

	@Override
	public String getModalidadePagto() {
		// TODO Auto-generated method stub
		return "CartaoCredito";
	}

	
	
}
