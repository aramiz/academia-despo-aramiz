package br.gov.serpro;

public class Principal {

	public static void main(String[] args) {
		Pilha p = new Pilha(10);
		p.empilhar("Abel");
		p.empilhar("Anselmo");
		p.empilhar("Adriana");
		p.empilhar("Ariane");
		System.out.println(p.topo());
		System.out.println(p.tamanho());

		
		//Desempilhando		
		System.out.println("----------");
		System.out.println(p.desempilhar());
		System.out.println(p.topo());
		System.out.println(p.tamanho());
		
	}

}
