package br.gov.serpro;

public class CtrlRemoto {
		
	private	Tv minhaTV;
	
	public CtrlRemoto(Tv minhaTV) {
		this.minhaTV = minhaTV;
	}
		
	public boolean ligarDesligarTv() {
		return minhaTV.alterarEstadoTv();		
	}
	
	public void aumentarVolumeTv() {
		minhaTV.aumentarVolume();
	}
	
	public void diminuirVolumeTv() {
		minhaTV.diminuirVolume();
	}
	
	public int mudarCanalTv(int canalCr) {
		   return minhaTV.alterarCanal(canalCr);
	}
	

}
