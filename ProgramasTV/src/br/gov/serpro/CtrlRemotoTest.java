package br.gov.serpro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CtrlRemotoTest {
     Tv minhaTv;
     CtrlRemoto controleRemoto;
     
	@BeforeEach
	public void informarDados() {
		int[] canais = {2, 4, 5, 7, 9, 11, 13, 16, 21, 32};
		minhaTv = new Tv(canais);
		
		controleRemoto = new CtrlRemoto(minhaTv);
	}
	
	@Test
	public void deveLigarTv() {
		assertEquals(true, controleRemoto.ligarDesligarTv());
		assertEquals(false, controleRemoto.ligarDesligarTv());
	}
	
		

}
