package br.gov.serpro;

public class Tv implements TelevisorControlavel{
	private static final int VOLUME_MAX = 50;
	private static final int VOLUME_MIN = 0;
	private int volume = 0;
	private boolean ligado = false;
	private int canalAtivo = 2;
	private int[] listaCanal; 
	private int indiceCanal = 0;

	public Tv(int[] listaCanal) { // int i, int j, int k, int l, int m, int n, int o, int p, int q, int r) {
		this.listaCanal = listaCanal;
	}

	public boolean alterarEstadoTv() {
		ligado = !ligado;
		return ligado;
	}

	public Integer aumentarVolume() {
		if (volume < VOLUME_MAX & ligado) {
			volume++;
			return volume;
		} else {
			return null;
		}
	}

	public Integer diminuirVolume() {
		if (volume > VOLUME_MIN & ligado) {
			volume--;
			return volume;
		} else {
			return null;
		}
	}

	public Integer alterarCanal(int canal) {
		if (ligado) {
			for (int i = 0; i < listaCanal.length; i++) {
				if (listaCanal[i] == canal) {
					indiceCanal = i;
					canalAtivo = listaCanal[i];
					return canalAtivo;
				}
			}
			return canalAtivo;
		} else {
			return null;
		}

	}

	public Integer aumentarCanal() {
		if (ligado) {
			if (indiceCanal == listaCanal.length) {
				indiceCanal = 0;
			} else {
				indiceCanal++;
			}
			canalAtivo = listaCanal[indiceCanal];
			return canalAtivo;
		} else {
			return null;
		}
	}

	public Integer diminuirCanal() {
		if (ligado) {
			if (indiceCanal == 0) {
				indiceCanal = listaCanal.length;
			} else {
				indiceCanal--;
			}
			canalAtivo = listaCanal[indiceCanal];
			return canalAtivo;
		} else {
			return null;
		}

	}
	
}
